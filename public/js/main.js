$(document).ready(function(){
    $('.delete-recipe').on('click', function(){
        // Note that the original code did not work - had to recode using the attr Ajax method to get at the data-id attribute!
        var id = $(this).attr("data-id");
        var url = '/delete/'+id;
        if (confirm('Delete Recipe?')){
            console.log('Calling Ajax...');
            $.ajax({
                url: url,
                type:'DELETE',
                success: function(result){
                    console.log('Deleting Recipe...');
                    window.location.href='/';
                },
                error: function(err){
                    console.log(err);
                }
            });
        }
        else {
            console.log('Did not confirm deletion!');
        }
    });

    $('.edit-recipe').on('click', function(){
        $('#edit-form-name').val($(this).data('name'));
        $('#edit-form-ingredients').val($(this).data('ingredients'));
        $('#edit-form-directions').val($(this).data('directions'));
        $('#edit-form-id').val($(this).data('id'));
        console.log('ID in JQUERY is : ' +$('#edit-form-id').val );
    })
});