var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cons = require('consolidate'),
    dust = require('dustjs-helpers'),
    app = express();

const {
    Client
} = require('pg');

// DB Connect String
var connectionString = "postgres://Kipper:Twiglets101@localhost/recipebookdb";

const client = new Client({
    connectionString: connectionString,
});

// Assisgn Dust Engine to .dust files
app.engine('dust', cons.dust);

// Set default Ext .dust
app.set('view engine', 'dust');
app.set('views', __dirname + '/views');

// Set Public folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));


app.get('/', function (req, res) {
    client.connect();
    client.query('select * from recipes', function (err, result) {
        if (err) {
            return console.error('error running query', err);
        }
        // The below is not releasing the connection!
        //client.end()
        res.render('index', {
            recipes: result.rows
        });
    });
});

app.post('/add', function (req, res) {
    client.connect();
    client.query('INSERT INTO recipes(name, ingredients, directions) VALUES($1, $2, $3)', [req.body.name, req.body.ingredients, req.body.directions]);
    //done();
    res.redirect('/');
});

app.delete('/delete/:id', function (req, res) {
    client.connect();
    // Note that reserved words for column names have to be in double quotes in PostGres - "ID"
    // Currently no error handling - need to add this.
    // Note that JavaScript kindly lets me embed double quotes within single quotes so no need to build the string in composite parts
    // The async query method allows for error handlng so will need to consult the PostGres documentation
    client.query('DELETE FROM recipes WHERE "ID" = $1', [req.params.id]);
    //done();
    res.sendStatus(200);
});

app.post('/edit', function (req, res) {
    client.connect();
    // Note that reserved words for column names have to be in double quotes in PostGres - "ID"
    // Currently no error handling - need to add this.
    // Note that JavaScript kindly lets me embed double quotes within single quotes so no need to build the string in composite parts
    // The async query method allows for error handlng so will need to consult the PostGres documentation
    console.log('Issuing update for : ' + 'name: ' + req.body.name);
    console.log('Issuing update for : ' + 'ingredients: ' + req.body.ingredients);
    console.log('Issuing update for : ' + 'directions: ' + req.body.directions);
    console.log('Issuing update for : ' + 'id: ' + req.body.id);

    client.query('UPDATE recipes SET name=$1, ingredients = $2, directions=$3 WHERE "ID" = $4', [req.body.name, req.body.ingredients, req.body.directions, req.body.id]);
    //done();
    res.redirect('/');
})

// Server
app.listen(3000, function () {
    console.log('Server started on port 3000');
});